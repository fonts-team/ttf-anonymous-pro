Source: fonts-anonymous-pro
Section: fonts
Priority: optional
Build-Depends:
 debhelper (>= 7.0.50~)
Maintainer: Debian Fonts Task Force <pkg-fonts-devel@lists.alioth.debian.org>
Uploaders:
 Rogério Brito <rbrito@ime.usp.br>,
 Christian Perrier <bubulle@debian.org>
Standards-Version: 3.9.2
Homepage: http://www.ms-studio.com/FontSales/anonymouspro.html

Package: ttf-anonymous-pro
Architecture: all
Depends:
 ${misc:Depends}
Description: fixed width sans serif font designed for coders
 Anonymous Pro (2009) is a family of four fixed-width fonts designed
 especially with coding in mind. Characters that could be mistaken for
 one another (O, 0, I, l, 1, etc.) have distinct shapes to make them
 easier to tell apart in the context of source code.
 .
 Anonymous Pro also features an international, Unicode-based character
 set, with support for most Western and European Latin-based languages,
 Greek, and Cyrillic. It also includes special “box drawing” characters
 for those who need them.
 .
 While Anonymous Pro looks great on Macs, Windows and Linux PCs with
 antialiasing enabled, it also includes embedded bitmaps for specific
 pixel sizes ("ppems" in font nerd speak) for both the regular and bold
 weight. (Since slanted bitmaps look pretty bad and hard to read at the
 supported sizes, it was chosen to use the upright bitmaps for the
 italics as well.) Bitmaps are included for these ppems: 10, 11, 12, and
 13.
